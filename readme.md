# Cinnamond
Cinnamond is a simple Cinnamon applet for monitoring the status of a Systemd-service. It works by regularly polling the status of the named service and displays the results on the Cinnamon taskbar. 

## Installation
1. Copy the entire directory to your Cinnamon applet path. Default being */home/USERNAME/.local/share/cinnamon/applets/*
2. Perform initial configuration by editing the *settings-schema.json*, for information about each setting, see the Configuration section in this readme.
3. Right click your panel and click *Applets*
4. Select the Cinnamond applet and click *Apply*

## Configuration
After the applet is running you can configure it using the cogs-icon in the cinnamon applet ui. Changes done here will be automatically hot-deployed.
The following settings are available:

| Setting        	| Description           														| Default  					|
| ------------- 	| -------------																	| -------------				|
| service-name      | Name of the service you want to monitor. 										| example@example.service 	|
| update-interval   | How often (in ms) should the status be polled.      							|   5000 					|
| icon-ok 			| Name of the cinnamon icon that should be shown if the service is active.      | channel-secure			|
| icon-nok 			| Name of the cinnamon icon that should be shown if the service is NOT active.	| channel-insecure			|

## Licensing
Cinnamond is free software distributed under the GPLv3 license. For more information, see COPYING.txt.