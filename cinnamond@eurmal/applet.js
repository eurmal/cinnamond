/* 
	Copyright © 2020 Zakarias Juto

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const Applet = imports.ui.applet;
const Mainloop = imports.mainloop;
const Lang = imports.lang;
const GLib = imports.gi.GLib;
const Settings = imports.ui.settings;
const UUID = "cinnamond@eurmal";

function CinnamondApplet(orientation, panel_height, instance_id) {
	this._init(orientation, panel_height, instance_id);
}

CinnamondApplet.prototype = {
	__proto__: Applet.IconApplet.prototype,

	_init: function(orientation, panel_height, instance_id) {
		Applet.IconApplet.prototype._init.call(this, orientation, panel_height, instance_id);

		this.update_interval = 5000;
		this.service_name = "example@example.service";
		this.icon_ok = "channel-secure";
		this.icon_nok = "channel-insecure";

		try {
			this.settings = new Settings.AppletSettings(this, UUID, this.instance_id); 
			this.settings.bindProperty(Settings.BindingDirection.IN, "service-name", "service_name", this._settings_changed, null);
			this.settings.bindProperty(Settings.BindingDirection.IN, "update-interval", "update_interval", this._settings_changed, null);
			this.settings.bindProperty(Settings.BindingDirection.IN, "icon-ok", "icon_ok", this._settings_changed, null);
			this.settings.bindProperty(Settings.BindingDirection.IN, "icon-nok", "icon_nok", this._settings_changed, null);
			this.set_applet_icon_name(this.icon_nok);
			this.set_applet_tooltip(_("Service " + this.service_name + " is NOT active."));
			this._settings_changed();
		} catch (e) {
			global.logError(e);
		}
	},

	on_applet_removed_from_panel: function() {
		if (this._updateLoopID) {
			Mainloop.source_remove(this._updateLoopID);
		}
	},

	_run_cmd: function(command) {
		try {
			const [result, stdout, stderr] = GLib.spawn_command_line_sync(command);
			if (stdout != null) {
				return stdout.toString();
			}
		}
		catch (e) {
			global.logError(e);
		}

		return "";
	},

	_get_status: function() {
		return this._run_cmd("systemctl status " + this.service_name);
	},

	_update_ui: function(isActive) {
		if (isActive) {
			this.set_applet_icon_name(this.icon_ok);
			this.set_applet_tooltip(_("Service " + this.service_name + " is active."));
		} else {
			this.set_applet_icon_name(this.icon_nok);
			this.set_applet_tooltip(_("Service " + this.service_name + " is NOT active."));
		}
	},

	_update_loop: function() {
		const status = this._get_status();
		this._update_ui(status.includes("Active: active"));
		this._updateLoopID = Mainloop.timeout_add(this.update_interval, Lang.bind(this, this._update_loop));
	},

	_settings_changed: function() {
		if (this._updateLoopID) {
			Mainloop.source_remove(this._updateLoopID);
		}
		this._update_loop();
	}
};

function main(metadata, orientation, panel_height, instance_id) {
	return new CinnamondApplet(orientation, panel_height, instance_id);
};
